/*
 LHIGP_evo.h -		        Header file specifying the life-history functions of
                            a structured intraguild predator feeding on a unstructured resource, 
                            an unstructured consumer, in addition to cannibalism on juvenile predators

                            Functional Response is Holling Type II with attack rate and handling time formulation
 
                            Layout for the i-state variables:
 
                                istate[0][0] :	Size
 
                            Layout for the environmental variables:
 
                                E[0] :	Resource Biomass
								E[1] :	Consumer Biomass
								E[2] :	Juvenile Biomass
								E[3] :	Mortality due to cannibalism
                            Note that for a correct calculation of the selection gradient, the mortality arising from
                            cannibalism needs to be specified as environment, instead of belonging to a type. This implies
                            that the cannibalistic mortality term is implemented as an impact variable and total mortality
                            is an environmental variable.
                            
                            Layout for the interaction (and output) variables:
 
                                I[0][0] : Total Resource Ingestion
                                I[0][1] : Total Consumer Ingestion
                                I[0][2] : Juvenile biomass
                                I[0][3] : Adult Biomass
                                I[0][4] : Mortality from cannibalism
								I[0][5] : Population level biomass juvenile production
 								I[0][6] : Population level biomass reproduction rate
 								I[0][7] : Mortality due to Cannibalism
								
                            Last modification: VH - Jan 23, 2017
*/

/*
 *===========================================================================
 * 		DEFINITION OF PROBLEM DIMENSIONS AND NUMERICAL SETTINGS
 *===========================================================================
 */
// Dimension settings: Required
#define POPULATION_NR		1
#define STAGES          2
#define	I_STATE_DIM     1
#define	ENVIRON_DIM     4
#define INTERACT_DIM		7
#define	PARAMETER_NR		20

// Numerical settings: Optional (default values adopted otherwise)
#define MIN_SURVIVAL		1.0E-9		// Survival at which individual is considered dead
#define MAX_AGE             10000		// Give some absolute maximum for individual age

#define DYTOL               1.0E-7		// Variable tolerance
#define RHSTOL              1.0E-6		// Function tolerance
#define ESSTOL				1.0E-8		// ESS tolerance

#define COHORT_NR           100
#define ALLOWNEGATIVE       0
#define FULLSTATEOUTPUT     0

/*
 *===========================================================================
 * 		DEFINITION OF ALIASES
 *===========================================================================
 */
// Define aliases for the istate variables
#define SIZE        istate[0][0]

// Define aliases for the environmental variables
#define R           E[0]            // Resource Biomass
#define C			E[1]            // Vulnerable consumer biomass
#define J           E[2]            // Juvenile Biomass
#define MORTCANN    E[3]            // Mortality due to cannibalism

// Define aliases for the parameters
#define RMAX		  parameter[ 0]	// Default: 3.0		Maximum Resource Density
#define DELTA		  parameter[ 1]	// Default: 1.0		Resource turn-over rate

#define ACR       parameter[ 2]	// Default: 10		Attack rate Consumer on Resource
#define HC			  parameter[ 3]	// Default: 0.1		Handling time Consumer
#define TC			  parameter[ 4]	// Default: 1.0		Maintenance rate Consumer
#define MUC   		parameter[ 5]   // Default: 0.1     Mortality Consumer

#define AJR			  parameter[ 6]	// Default: 4.0		Attack rate Juvenile on Resource
#define AAR			  parameter[ 7]	// Default: 3.0		Attack rate Adults on Resource
#define AP	    	parameter[ 8]	// Default: 6.0		Attack rate in Trade-off
#define EPS	    	parameter[ 9]	// Default: 0.0		Trade-off strength scalar
#define HP        parameter[10] // Default: 0.25    Handling time Predator
#define TP			  parameter[11]	// Default: 0.4 	Maintenance Predator

#define MUP   		parameter[12]   // Default: 0.04    Mortality Predator
#define MUJ       parameter[13]   // Default: 0.0     Additional Juvenile Mortality
#define MUA       parameter[14]   // Default: 0.0     Additional Adult Mortality

#define SIGMA		  parameter[15]	// Default: 0.5		Food conversion efficiency

#define B	    	  parameter[16]	// Default: 0		Cannibalistic scalar
#define MUCANN    parameter[17]	// Default: 0		Cost of cannibalism

#define	SB			  parameter[18]	// Default: 0.01	Newborn size
#define SM			  parameter[19]	// Default: 1.0		Adult mass

/*
 *===========================================================================
 * 		DEFINITION OF NAMES AND DEFAULT VALUES OF THE PARAMETERS
 *===========================================================================
 */
// At least two parameters should be specified in this array
char  *parameternames[PARAMETER_NR] =
    {"RMAX", "DELTA", "ACR", "HC", "TC", "MUC", "AJR", "AAR", "AP", "EPS", "HP", "TP", "MUP", "MUJ", "MUA", "SIGMA", "B", "MUCANN", "SB", "SM"};

// These are the default parameters values from the SSBM with time unit 10 days.
double	parameter[PARAMETER_NR] =
    {3.0, 1.0, 10, 0.1, 1.0, 0.1, 3.0, 3.0, 6.0, 0.0, 0.25, 0.4, 0.04, 0.0, 0.0, 0.5, 0, 0, 0.01, 1.0};

/*
 *===========================================================================
 * 		DEFINITION OF THE LIFE HISTORY MODELS FOLLOWS BELOW
 *===========================================================================
 * Specify the number of states at birth for the individuals in all structured
 * populations in the problem in the vector BirthStates[].
 *===========================================================================
 */

void SetBirthStates(int BirthStates[POPULATION_NR], double E[])
{
    BirthStates[0] = 1;
    
    return;
}

/*
 *===========================================================================
 * Specify all the possible states at birth for all individuals in all
 * structured populations in the problem. BirthStateNr represents the index of
 * the state of birth to be specified. Each state at birth should be a single,
 * constant value for each i-state variable.
 *
 * Notice that the first index of the variable 'istate[][]' refers to the
 * number of the structured population, the second index refers to the
 * number of the individual state variable. The interpretation of the latter
 * is up to the user.
 *===========================================================================
 */

void StateAtBirth(double *istate[POPULATION_NR], int BirthStateNr, double E[])
{
    
    SIZE = SB;
    
    return;
}


/*
 *===========================================================================
 * Specify the threshold determining the end point of each discrete life
 * stage in individual life history as function of the i-state variables and
 * the individual's state at birth for all populations in every life stage.
 *
 * Notice that the first index of the variable 'istate[][]' refers to the
 * number of the structured population, the second index refers to the
 * number of the individual state variable. The interpretation of the latter
 * is up to the user.
 *===========================================================================
 */

void IntervalLimit(int lifestage[POPULATION_NR], double *istate[POPULATION_NR],
                   double *birthstate[POPULATION_NR], int BirthStateNr, double E[],
                   double limit[POPULATION_NR])
{
    switch (lifestage[0])
    {
        case 0:
            limit[0] = SIZE - SM;
            break;
    }
    
    return;
}




/*
 *===========================================================================
 * Specify the individual development of individuals as function of i-state
 * and environment for all individuals of all populations in every life stage
 *
 * Notice that the first index of the variables 'istate[][]' and 'growth[][]'
 * refers to the number of the structured population, the second index refers
 * to the number of the individual state variable. The interpretation of the
 * latter is up to the user.
 *===========================================================================
 */

void Development(int lifestage[POPULATION_NR], double *istate[POPULATION_NR],
            double *birthstate[POPULATION_NR], int BirthStateNr, double E[],
            double growth[POPULATION_NR][I_STATE_DIM])
{
    double  nuj = SIGMA * AJR * R / (1 + AJR * HP * R) - TP;
	double 	nujplus = max(nuj, 0.0);
    
    if (lifestage[0] == 0)
    {
        growth[0][0]    = nujplus * SIZE;
    }
    else
    {
        growth[0][0]    = 0.0;
    }
    
    return;
}


/*
 *===========================================================================
 * Specify the possible discrete changes (jumps) in the individual state
 * variables when ENTERING the stage specified by 'lifestage[]'.
 *
 * Notice that the first index of the variables 'istate[][]' and 'growth[][]'
 * refers to the number of the structured population, the second index refers
 * to the number of the individual state variable. The interpretation of the
 * latter is up to the user.
 *===========================================================================
 */

void DiscreteChanges(int lifestage[POPULATION_NR], double *istate[POPULATION_NR],
                     double *birthstate[POPULATION_NR], int BirthStateNr, double E[])
{
    return;
}


/*
 *===========================================================================
 * Specify the fecundity of individuals as a function of the i-state
 * variables and the individual's state at birth for all populations in every
 * life stage.
 *
 * The number of offspring produced has to be specified for every possible
 * state at birth in the variable 'fecundity[][]'. The first index of this
 * variable refers to the number of the structured population, the second
 * index refers to the number of the birth state.
 *
 * Notice that the first index of the variable 'istate[][]' refers to the
 * number of the structured population, the second index refers to the
 * number of the individual state variable. The interpretation of the latter
 * is up to the user.
 *===========================================================================
 */

void Fecundity(int lifestage[POPULATION_NR], double *istate[POPULATION_NR],
               double *birthstate[POPULATION_NR], int BirthStateNr, double E[],
               double *fecundity[POPULATION_NR])
{
  
    double AAC = max( (AP - AJR) / (1 + EPS * AJR / AP), 0.0);
    
    double nua = SIGMA * (AAR * R + AAC * (C + B * J)) / (1 + HP * (AAR * R + AAC * (C + B * J))) - TP;
	double nuaplus = max(nua, 0.0);
    
    if (lifestage[0] == 0)
    {
        fecundity[0][0] = 0.0;
    } 
    else 
    {
        fecundity[0][0] = nuaplus * SM / SB;
    }
    
    return;
}



/*
 *===========================================================================
 * Specify the mortality of individuals as a function of the i-state
 * variables and the individual's state at birth for all populations in every
 * life stage.
 *
 * Notice that the first index of the variable 'istate[][]' refers to the
 * number of the structured population, the second index refers to the
 * number of the individual state variable. The interpretation of the latter
 * is up to the user.
 *===========================================================================
 */

void Mortality(int lifestage[POPULATION_NR], double *istate[POPULATION_NR],
               double *birthstate[POPULATION_NR], int BirthStateNr, double E[],
               double mortality[POPULATION_NR])
{
    double AAC = max( (AP - AJR) / (1 + EPS * AJR / AP), 0.0);    
   
    double COSTCANN = (MUCANN * AAC * B * J) / (1 + HP * (AAR * R + AAC * (C + B * J)));
    
    double  nuj = SIGMA * AJR * R / (1 + AJR * HP * R) - TP;
	double 	nujmin = min(nuj, 0.0);
	
    double  nua = SIGMA * (AAR * R + AAC * (C + B * J)) / (1 + HP * (AAR * R + AAC * (C + B * J))) - TP;
	double  nuamin = min(nua, 0.0);
	
    if (lifestage[0] == 0)
    {
        mortality[0] = MUP + MUJ + MORTCANN - nujmin;
    } 
    else
    {
        mortality[0] = MUP + MUA + COSTCANN - nuamin;
    }
	
    return;
}


/*
 *===========================================================================
 * For all the integrals (measures) that occur in interactions of the
 * structured populations with their environments and for all the integrals
 * that should be computed for output purposes (e.g. total juvenile or adult
 * biomass), specify appropriate weighing function dependent on the i-state
 * variables, the environment variables and the current life stage of the
 * individuals. These weighing functions should be specified for all
 * structured populations in the problem. The number of weighing functions
 * is the same for all of them.
 *
 * Notice that the first index of the variables 'istate[][]' and 'impact[][]'
 * refers to the number of the structured population, the second index of the
 * variable 'istate[][]' refers to the number of the individual state variable,
 * while the second index of the variable 'impact[][]' refers to the number of
 * the interaction variable. The interpretation of these second indices is up
 * to the user.
 *===========================================================================
 */

void Impact(int lifestage[POPULATION_NR], double *istate[POPULATION_NR],
            double *birthstate[POPULATION_NR], int BirthStateNr, double E[],
            double impact[POPULATION_NR][INTERACT_DIM])
{
    double IJR;
    double IAR, IAC;
    double prod, repro, repro_plus, mort;
    
    double AAC = max( (AP - AJR) / (1 + EPS * AJR / AP), 0.0);
   
    IJR = AJR * R / (1 + AJR * HP * R);

    IAR = AAR * R / (1 + HP * (AAR * R + AAC * (C + B * J)));
    IAC = AAC / (1 + HP * (AAR * R + AAC * (C + B * J)));
    
    prod = SIGMA * AJR * R / (1 + AJR * HP * R) - TP;
    mort = MUP + MUJ + MORTCANN;
    repro = SIGMA * (AAR * R + AAC * (C + B * J)) / (1 + HP * (AAR * R + AAC * (C + B * J))) - TP;
	repro_plus = max(repro, 0.0);

    switch (lifestage[0])
    {
        case 0:
            impact[0][0] = IJR * SIZE;                                              // IJR
            impact[0][1] = 0.0;     			                                    // IJC
            impact[0][2] = SIZE;                                     				// Juvenile Biomass
            impact[0][3] = 0.0;                                     				// Adult Biomass
            impact[0][4] = (prod - mort) * SIZE;                                    // Population level Juvenile Production
            impact[0][5] = 0.0;
            impact[0][6] = 0.0;
			break;
        case 1:
            impact[0][0] = IAR * SM;	                                            // IAR
            impact[0][1] = IAC * SM;		                                        // IAC
            impact[0][2] = 0.0;                                 					// Juvenile Biomass
            impact[0][3] = SM;                                   					// Adult Biomass
            impact[0][4] = 0.0;
			impact[0][5] = repro_plus * SM;                                         // Population Level Reproduction
			impact[0][6] = AAC * B * SM / (1 + HP * (AAR * R + AAC * (C + B * J))); // Cannibalistic mortality
            break;

        /* In equilibrium the following equilities holds; 
             *
             *  Population Level Maturation - Population Level Reproduction = Total Juvenile Production - Total Juvenile Mortality      
             *  I[0][4] calculates the RHS.
             *  I[0][5] calculates the population level reproduction
             *  Population Level Maturation follows from I[0][6] + I[0][5]
             *
             */
    }
    return;
}


/*
 *===========================================================================
 * Specify the type of each of the environment variables by setting
 * the entries in EnvironmentType[ENVIRON_DIM] to PERCAPITARATE, GENERALODE
 * or POPULATIONINTEGRAL based on the classification below:
 *
 * Set an entry to PERCAPITARATE if the dynamics of E[j] follow an ODE and 0
 * is a possible equilibrium state of E[j]. The ODE is then of the form
 * dE[j]/dt = P(E,I)*E[j], with P(E,I) the per capita growth rate of E[j].
 * Specify the equilibrium condition as condition[j] = P(E,I), do not include
 * the multiplication with E[j] to allow for detecting and continuing the
 * transcritical bifurcation between the trivial and non-trivial equilibrium.
 *
 * Set an entry to GENERALODE if the dynamics of E[j] follow an ODE and 0 is
 * NOT an equilibrium state of E. The ODE then has a form dE[j]/dt = G(E,I).
 * Specify the equilibrium condition as condition[j] = G(E,I).
 *
 * Set an entry to POPULATIONINTEGRAL if E[j] is a (weighted) integral of the
 * population distribution, representing for example the total population
 * biomass. E[j] then can be expressed as E[j] = I[p][i]. Specify the
 * equilibrium condition in this case as condition[j] = I[p][i].
 *
 * Notice that the first index of the variable 'I[][]' refers to the
 * number of the structured population, the second index refers to the
 * number of the interaction variable. The interpretation of the latter
 * is up to the user. Also notice that the variable 'condition[j]' should
 * specify the equilibrium condition of environment variable 'E[j]'.
 *===========================================================================
 */

const int EnvironmentType[ENVIRON_DIM] = {GENERALODE, PERCAPITARATE, POPULATIONINTEGRAL, POPULATIONINTEGRAL};

void EnvEqui(double E[], double I[POPULATION_NR][INTERACT_DIM],
             double condition[ENVIRON_DIM])
{
    condition[0] = DELTA * (RMAX - R) - I[0][0] - ACR * R * C / (1 + HC * ACR * R);   // Resource dynamics
    condition[1] = SIGMA * ACR * R / (1 + HC * ACR * R) - TC - MUC - I[0][1];         // Consumer dynamics
    condition[2] = I[0][2];															  // Juvenile Biomass
    condition[3] = I[0][6];															  // Mortality due to cannibalism

    return;
}

/*==============================================================================*/