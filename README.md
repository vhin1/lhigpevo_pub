# LHIGPevo_pub
Code from: Hin &amp; De Roos Cannibalism prevents evolutionary suicide of ontogenetic omnivores in life‐history intraguild predation systems. Ecology & Evolution. 2019;00:1–16. https://doi.org/10.1002/ece3.5004
 
Execution of the code requires the installation of PSPManalysis package
See: https://bitbucket.org/amderoos/pspmanalysis/src/master/ for more info

 - Hin&DeRoos_LHIGPevo_analysis.R contains commands to run bifurcations as a function of parameter AJR and produce Figures 2, 4 & 6
 - Hin&DeRoos_LHIGPevo_twopar.R contains commands to run two-parameter continuations (AJR & B) and produce Figures 3, 5 & 7
 - Hin&DeRoos_LHIGPevo_functions.R contains functions and parameter settings and is sourced from (...)_analysis.R & (...)_twopar.R
 - LHIGP_evo.h is a C-header file that contains the model implementation and should not be changed.

Code is released under GNU Public License, Version 3. A copy of the license is attached.

Please cite the above paper when reusing any part of the code in a publication
 
Last modified: VH - 12 March 2019